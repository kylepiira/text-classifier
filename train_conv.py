﻿# Keras
from keras.models import Sequential
from keras.layers import Dense, Embedding, Conv1D, Flatten
from keras.utils import to_categorical
from keras.callbacks import TensorBoard, ModelCheckpoint
# SentencePiece
import sentencepiece as spm
# Scipy
import numpy as np
# General
from tqdm import tqdm
import os

# Settings
max_seq_length = 50
batch_size = 128
epochs = 5

# Load Tokenizer
sp = spm.SentencePieceProcessor()
sp.Load("tokenizer/english.model")

# Init categories
categories = [fn.replace('.txt','') for fn in os.listdir('data')]

# Init training
x, y = [], []

# Load Data
for category in categories:
	with open('data/' + category + '.txt') as source:
		print(category)
		for text in tqdm(source):
			array = np.zeros(max_seq_length)
			toks = sp.EncodeAsIds(text)
			array[:len(toks)] = toks[:max_seq_length]
			# Add to training queue
			x.append(array)
			y.append(categories.index(category))

# Shuffle the training data
x, y = np.array(x), np.array(y)
p = np.random.permutation(len(x))
x, y = x[p], y[p]

# Convert targets to categories
y = to_categorical(y)

# Build Model
model = Sequential()
model.add(Embedding(35000, 64, input_length=max_seq_length))
model.add(Conv1D(512, 2, padding='same'))
model.add(Conv1D(512, 2, padding='same'))
model.add(Conv1D(512, 2, padding='same'))
model.add(Flatten())
model.add(Dense(len(categories), activation='softmax'))

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])

# Train Model
model.fit(
	x, y,
	batch_size=batch_size,
	epochs=epochs,
	validation_split=0.10,
	callbacks=[
		TensorBoard(log_dir='logs'),
		ModelCheckpoint('checkpoint.model')
	],
)

model.save('classifier.model')