﻿# Keras
from tensorflow.keras.models import load_model
# SentencePiece
import sentencepiece as spm
# Scipy
import numpy as np
# General
from tqdm import tqdm
import os
import json

# Settings
max_seq_length = 50

# Load Tokenizer
sp = spm.SentencePieceProcessor()
sp.Load("tokenizer/english.model")

# Load Model
model = load_model('classifier.h5')

# Init categories
categories = np.array([fn.replace('.txt','') for fn in os.listdir('data')])

def tokenize_text(text):
	array = np.zeros(max_seq_length)
	toks = sp.EncodeAsIds(text)
	array[:len(toks)] = toks[:max_seq_length]
	return np.array([array])

def tokenize_texts(texts):
	array = np.zeros((len(texts), max_seq_length))
	for i, text in enumerate(texts):
		tokens = tokenize_text(text)[0]
		array[i][:len(tokens)] = tokens[:max_seq_length]
	return array

def predict_text(text):
	prediction = model.predict(tokenize_text(text))[0]
	return categories[np.argmax(prediction)]

if __name__ == '__main__':
	with open('input.txt') as texts:
		queue = []
		for text in tqdm(texts):
			queue.append(text)

			if len(queue) >= 500:
				# Predict if from Wikipedia or Quora
				predictions = model.predict(tokenize_texts(queue))
				predicted_cats = categories[np.argmax(predictions, axis=1)]
				# Print QA pair if detected from Wikipedia
				for i, predicted_cat in enumerate(predicted_cats):
					if predicted_cat == 'real':
						with open('real.txt','a+') as real_f:
							real_f.write(queue[i])

				queue = []