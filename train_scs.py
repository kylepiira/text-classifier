﻿# Keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Embedding, Lambda
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint
import tensorflow.keras.backend as K
# SentencePiece
import sentencepiece as spm
# Scipy
import numpy as np
# General
from tqdm import tqdm
import os

# Settings
max_seq_length = 50
batch_size = 128
embedding_dim = 4
epochs = 5

# Load Tokenizer
sp = spm.SentencePieceProcessor()
sp.Load("tokenizer/english.model")

# Init categories
categories = [fn.replace('.txt','') for fn in os.listdir('data')]

# Init training
x, y = [], []

# Load Data
for category in categories:
	with open('data/' + category + '.txt') as source:
		print(category)
		for text in tqdm(source):
			array = np.zeros(max_seq_length)
			toks = sp.EncodeAsIds(text)
			array[:len(toks)] = toks[:max_seq_length]
			# Add to training queue
			x.append(array)
			y.append(categories.index(category))

# Shuffle the training data
x, y = np.array(x), np.array(y)
p = np.random.permutation(len(x))
x, y = x[p], y[p]

# Convert targets to categories
y = to_categorical(y)

# Build Model
model = Sequential()
model.add(Embedding(len(sp), embedding_dim, mask_zero=True))
model.add(Lambda(lambda x: K.sum(x, axis=1)))
model.add(Dense(len(categories), activation='softmax'))
# Compile Model
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])
# Print Model to Term
model.summary()

# Train Model
model.fit(
	x, y,
	batch_size=batch_size,
	epochs=epochs,
	validation_split=0.10,
	callbacks=[],
)

model.save('classifier.h5')